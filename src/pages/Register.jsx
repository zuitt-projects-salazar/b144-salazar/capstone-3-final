import styled from "styled-components";
import { mobile } from "../responsive";
// import { UserContext } from '../UserContext';
import { useState, useEffect, useContext } from 'react';
import { Redirect, useHistory, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import { useDispatch, useSelector } from "react-redux";
import { register } from "../redux/apiCalls";


const Container = styled.div`
  width: 100vw;
  height: 100vh;
  background:
    url("https://c4.wallpaperflare.com/wallpaper/357/77/305/neon-genesis-evangelion-ayanami-rei-asuka-langley-soryu-ikari-shinji-wallpaper-preview.jpg")
      center;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Wrapper = styled.div`
  width: 37%;
  padding: 20px;
  background-color: #a5bdd6;
  ${mobile({ width: "75%" })}
`;

const Title = styled.h1`
  font-size: 24px;
  font-weight: 300;
`;

const Form = styled.form`
  display: flex;
  flex-wrap: wrap;
`;

const Input = styled.input`
  flex: 1;
  min-width: 40%;
  margin: 20px 10px 0px 0px;
  padding: 10px;
`;

const Agreement = styled.span`
  font-size: 12px;
  margin: 20px 0px;
`;

const Button = styled.button`
  margin: 2% 2% 0% 0%;
  height: 30%;
  width: 30%;
  border: none;
  padding: 10px 20px;
  background-color: #6e3e58;
  color: white;
  cursor: pointer;
`;

const Link2 = styled.a`
  margin: 5px 0px;
  font-size: 12px;
  text-decoration: underline;
  cursor: pointer;
`;

const Button2 = styled.button`
  margin: 5% 0 0 60%;
  height: 90%;
  width: 90%;
  border: none;
  padding: 10px; 20px;
  background-color: #6e3e58;
  color: white;
  cursor: pointer;
`;


const Register = () => {

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const dispatch = useDispatch();
  const [isActive, setIsActive] = useState(false);
   const { isFetching, error } = useSelector((state) => state.user);
   const password = password1;

   const handleClick = (e) => {
    e.preventDefault();
    register(dispatch, { firstName, lastName, email, username, password1 });
  };


useEffect(() => {
    // Validation to enable the submit buttion when all fields are populated and both passwords match
    if((firstName !== '' && lastName !== ''&& username !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
      setIsActive(true);
    }
    else{
      setIsActive(false);
    }
  }, [firstName, lastName, username, email, password1, password2])

  return (

    <Container>
      <Wrapper>
        <Title>SIGN IN</Title>
        <Form>
          <Input
            placeholder="FirstName"
            onChange={(e) => setFirstName(e.target.value)}
          />
          <Input
            placeholder="LastName"
            onChange={(e) => setLastName(e.target.value)}
          />
          <Input
            placeholder="username"
            onChange={(e) => setUsername(e.target.value)}
          />
        
          <Input
            placeholder="email"
            onChange={(e) => setEmail(e.target.value)}
          />
          <Input type="password"
            placeholder="Password"
            onChange={(e) => setPassword1(e.target.value)}
          />
          <Input type="password"
            placeholder="Confirm Password"
            onChange={(e) => setPassword2(e.target.value)}
          />
          <Button onClick={handleClick} disabled={isFetching}>
            REGISTER
          </Button>
         <Link to="/login" >
          <Button2>
          ALREADY HAVE AN ACCOUNT?
          </Button2>
          </Link>
        </Form>
      </Wrapper>
    </Container>
  );
};

export default Register;

