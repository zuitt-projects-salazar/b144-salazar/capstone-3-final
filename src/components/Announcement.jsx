import styled from "styled-components";

const Container = styled.div`
  height: 30px;
  background-color: teal;
  color: white;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 14px;
  font-weight: 500;
`;

const Announcement = () => {
  return <Container>Anywhere can be paradise as long as you have the will to live. After all, you are alive, so you will always have the chance to be happy.</Container>;
};

export default Announcement;
